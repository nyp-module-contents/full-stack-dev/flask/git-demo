from flask import Flask
import os

server = Flask(__name__,
        # Where the templates "html files" are located relative to cwd
       template_folder = os.path.join(os.getcwd(), "templates"),
        # Where the static files aka "public" are located relative to cwd
       static_folder   = os.path.join(os.getcwd(), "public"),
        # The target host url on your server. http://localhost:5000/public
       static_url_path = "/public")

from services.common import endpoint as EP_Common
from services.admin  import endpoint as EP_Admin
from services.andrea import endpoint as EP_andrea
# Assign all the endpoints in EP_Common to http://localhost:3000/
server.register_blueprint(EP_Common, url_prefix="/")
# Assign all the endpoints in EP_Admin  to http://localhost:3000/admin
server.register_blueprint(EP_Admin,  url_prefix="/admin")

server.register_blueprint(EP_andrea,  url_prefix="/andrea")

#   Main Entry Point
#   Where everything begins
if (__name__ == "__main__"):
    print(f"My Current working directory is :{os.getcwd()}")
    print(f"My templates are found at :      {os.path.join(os.getcwd(), 'templates')}")
    server.run(host="0.0.0.0", port=5000)