//  Javascript entry point
window.addEventListener("load", function(event) {
    console.log("Initialized about.js");

    //  Find element with id "text-space" change the content to "Hello World"
    const element = document.getElementById("text-space");
    element.innerHTML = "Hello World";
});
