//  Javascript entry point
window.addEventListener("load", function(event) {
    console.log("Initialized main.js");
});

////  This line runs immediately after inclusion
////  OR aka <script src="...."> </script>
//document.getElementById("Test");    //  undefined
//
//function whatever() {
//    ....
//}