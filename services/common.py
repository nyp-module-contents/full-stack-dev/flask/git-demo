from flask import Blueprint
from flask import render_template

endpoint = Blueprint("common", __name__)

@endpoint.route("/")
def page_home():
    return render_template("home.html")

@endpoint.route("/about")
def page_about():
    return render_template("about.html")

